class Sort {
  filterCardsArr(arr, searchInput, status, priority) {
    const searchQuery = searchInput.toLowerCase()
    const filteredCards = arr.filter((card) => {
      const titleMatch = card.title.toLowerCase().includes(searchQuery)
      const descriptionMatch = card.description
        .toLowerCase()
        .includes(searchQuery)
      let statusMatch
      switch (status) {
        case 'Усі':
          statusMatch = true
          break
        default:
          statusMatch = card.status === status
      }
      let urgencyMatch
      switch (priority) {
        case 'Усі':
          urgencyMatch = true
          break
        default:
          urgencyMatch = card.priority === priority
      }
      return (titleMatch || descriptionMatch) && statusMatch && urgencyMatch
    })

    const cards = [...document.querySelectorAll('.visit-card-list')]
    cards.forEach((card) => {
      card.classList.add('hidden')
      let idElement = card.querySelector('.id').textContent
      filteredCards.forEach(({ id }) => {
        if (id === +idElement) {
          card.classList.remove('hidden')
        }
      })
    })

    return filteredCards
  }
}

// Filter Card перенести в файл function.

export { Sort }

import { Request } from "./request.js";
import {
  allCardVisit,
  arrCard,
  currentDate,
  filterCard,
  firstInfo,
} from "./functions.js";
import { Card } from "./card.js";
import { container, root, row } from "./constants.js";

//Створення дати поточної для записи на визит

export class Visit {
  #description;
  #title;
  #date;
  #patientLastName;
  #patientName;
  #priority;
  #age;
  #pressure;
  #imt;
  #illnesses;
  #lastVisitDate;
  #doctorLastName;
  #doctorName;

  render(
    id,
    status,
    doctor = "",
    title = "",
    doctorName = "",
    doctorLastName = "",
    description = "",
    patientName = "",
    patientLastName = "",
    priority = "",
    plannedDate = "",
    card,
    obj
  ) {
    const tempMeinInfo = document.querySelector("#main-info");
    const meininfo = tempMeinInfo.content.cloneNode(true);
    const otherInformation = document.createElement("div");
    this.newVisit = document.createElement("form");

    this.newVisit.prepend(meininfo);

    this.#title = this.newVisit.querySelector(".title");
    this.#description = this.newVisit.querySelector(".description");
    this.#date = this.newVisit.querySelector(".plannedDate");
    this.#date.addEventListener("change", () => {
      let blockDate = this.newVisit.querySelector(".after");
      let timeValue = new Date(this.#date.value).getTime();
      let date = new Date().getTime();
      if (timeValue < date) {
        let par = document.createElement("p");
        par.textContent = "Дата та час не можу бути менше за поточну";
        par.style.color = "red";
        blockDate.after(par);
        setTimeout(() => {
          par.remove();
        }, 2000);
        return (this.#date.value = currentDate());
      }
    });
    this.#patientLastName = this.newVisit.querySelector(".patientLastName");
    this.#patientName = this.newVisit.querySelector(".patientName");
    this.#priority = this.newVisit.querySelectorAll(".priority");
    this.#doctorLastName = this.newVisit.querySelector(".doctorLastName");
    this.#doctorName = this.newVisit.querySelector(".doctorName");
    this.select = this.newVisit.querySelector("#specialization");

    // Відображення попередніх значень полей при редагуванні картки
    this.#title.value = title;

    this.#description.value = description;
    this.#patientLastName.value = patientLastName;
    this.#patientName.value = patientName;
    this.#doctorLastName.value = doctorLastName;
    this.#doctorName.value = doctorName;

    if (plannedDate !== "") {
      this.#date.value = plannedDate;
    } else {
      // Відображення поточної дати, якщо запланована не була вказана
      this.#date.value = currentDate();
    }

    const selectOptions = [...this.select.options];
    selectOptions.find((item) => {
      if (item.innerText === doctor) {
        item.selected = "selected";
        this.selectDoctor(this.select, otherInformation);
      }
    });

    const priorityOption = [...this.#priority];
    priorityOption.find((item) => {
      if (item.value === priority) {
        item.checked = true;
      }
    });

    const modalCloseBtn = document.querySelector(".btn-close");
    this.buttonSubmit = document.createElement("button");
    this.buttonSubmit.type = "submit";
    this.buttonSubmit.className = "btn btn-primary mx-5";
    this.buttonSubmit.setAttribute("data-bs-dismiss", "modal");
    this.buttonSubmit.textContent = "Зберегти";

    const buttonClose = document.createElement("button");
    buttonClose.type = "button";
    buttonClose.className = "btn btn-secondary";
    buttonClose.setAttribute("data-bs-dismiss", "modal");
    buttonClose.textContent = "Закрить";

    buttonClose.addEventListener("click", () => {
      this.newVisit.remove();
    });
    modalCloseBtn.addEventListener("click", () => {
      this.newVisit.remove();
    });

    this.newVisit.append(otherInformation, buttonClose, this.buttonSubmit);

    this.newVisit.addEventListener("submit", (el) => {
      el.preventDefault();

      // Перевірка чи існує на сервері карточка з таким ID
      let checkCardRequest = new Request();
      checkCardRequest
        .getOneVisit(sessionStorage.getItem("token"), id)
        .then((data) => {
          if (data === 200) {
            let editRequest = new Request();
            editRequest
              .editPost(
                sessionStorage.getItem("token"),
                id,
                this.createObject(this.newVisit)
              )
              .then((data) => {
                row.innerHTML = "";
                const editedCard = new Card();
                editRequest
                  .getVisits(sessionStorage.getItem("token"))
                  .then((log) => {
                    allCardVisit(log);

                    const indexObjectEdit = arrCard.findIndex(
                      (obj) => obj.id === id
                    );
                    if (indexObjectEdit !== -1) {
                      arrCard[indexObjectEdit] = data;
                    }
                    filterCard(log);
                  });
                this.newVisit.remove();
              });

            // если не редактировать, а создавать новый тикет
          } else {
            this.createNewPost(this.createObject(this.newVisit));
            this.newVisit.remove();
          }
        });
    });

    this.select.addEventListener("change", () => {
      this.selectDoctor(this.select, otherInformation);

      this.getOtherInformation(this.newVisit);
    });

    for (let key in obj) {
      if (obj[key] !== undefined) {
        let teg = this.newVisit.querySelector(`.${key}`);
        teg.value = obj[key];
      }
    }
    return this.newVisit;
  }

  getOtherInformation(teg) {
    this.#age = teg.querySelector(".age");
    this.#pressure = teg.querySelector(".pressure");
    this.#imt = teg.querySelector(".imt");
    this.#illnesses = teg.querySelector(".illnesses");
    this.#lastVisitDate = teg.querySelector(".lastVisitDate");
  }

  selectDoctor(select, otherInformation) {
    let name = select.options[select.selectedIndex].text;

    switch (name) {
      case "Кардіолог":
        otherInformation.innerHTML = "";
        otherInformation.append(cardiolog.render());
        break;
      case "Терапевт":
        otherInformation.innerHTML = "";
        otherInformation.append(therapist.render());

        break;
      case "Дантист":
        otherInformation.innerHTML = "";
        otherInformation.append(dentist.render());
        break;
    }
    return otherInformation;
  }

  createNewPost(obj) {
    let request = new Request();

    request.creatNewPost(sessionStorage.getItem("token"), obj).then((date) => {
      let card = new Card();

      row.prepend(card.renderCard(date));

      container.append(row);
      root.append(container);

      const element = document.querySelector(".container-fluid");
      if (element) {
        firstInfo.remove();
      }
      arrCard.push(date);
      filterCard(arrCard);
    });
  }

  createObject(form) {
    let obj = {};
    let elements = [...form.elements];

    elements.forEach((element) => {
      switch (element.type) {
        case "radio":
          if (element.checked) {
            obj[element.name] = element.value;
          }
          break;
        default:
          obj[element.name] = element.value;
          break;
      }
    });

    return obj;
  }
}

class VisitDentist extends Visit {
  render() {
    const temp = document.querySelector("#dantist");
    const dantist = temp.content.cloneNode(true);

    return dantist;
  }
}

class VisitCardiologist extends Visit {
  render() {
    const temp = document.querySelector("#cardiolog");
    const cardiolog = temp.content.cloneNode(true);
    cardiolog.prepend(therapist.render());
    return cardiolog;
  }
}

class VisitTherapist extends Visit {
  render() {
    const temp = document.querySelector("#therapist");
    const therapist = temp.content.cloneNode(true);

    return therapist;
  }
}

let dentist = new VisitDentist();
let therapist = new VisitTherapist();
let cardiolog = new VisitCardiologist();

'use strict'

import { afterUserLogin, userLoginModal } from './functions.js'

const buttonContent = document.querySelector('.buttons')
const logOut = document.getElementById(`log-out`)

class Button {
  render(id, text) {
    const button = document.createElement('button')
    button.id = id
    button.type = 'button'
    button.className = 'btn btn-primary btn-lg'
    button.setAttribute('data-bs-toggle', 'modal')
    button.setAttribute('data-bs-target', '#exampleModal')
    button.textContent = text
    return button
  }
}

const button = new Button()

buttonContent.append(button.render('loginButton', 'Вхід'))
const login = document.querySelector('#loginButton')

login.addEventListener('click', userLoginModal)

if (sessionStorage.getItem('token')) {
  afterUserLogin(sessionStorage.getItem('token'), login)
}

logOut.addEventListener(`click`, () => {
  sessionStorage.removeItem('token')
  window.location.href = 'index.html'
})

export { Button, login, buttonContent }

import { URL } from './constants.js'

class Request {
  async getToken(loginUser, passwordUser) {
    const response = await fetch(
      'https://ajax.test-danit.com/api/v2/cards/login',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: loginUser,
          password: passwordUser,
        }),
      }
    )
    if (response.status === 401) {
      throw 'Логін або пароль не вірні'
    } else {
      return response.text()
    }
  }

  async getVisits(token) {
    const response = await fetch(URL, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return await response.json()
  }

  async getOneVisit(token, id){
    const response = await fetch(URL + `/${id}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })
    return response.status;
  }

  async creatNewPost(token, obj) {
    const response = await fetch(URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(obj),
    })
    return await response.json()
  }

  async editPost(token, postID, obj) {
    const response = await fetch(`${URL}/${postID}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(obj),
    })
    return response.json();
  }

  async deletePost(token, postID) {
    const response = await fetch(`${URL}/${postID}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return response.status;
  }
}
export { Request }

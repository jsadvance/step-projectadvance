const URL = "https://ajax.test-danit.com/api/v2/cards";
const root = document.querySelector("#root");

const templateFilterForm = document.getElementById(`search-form`);
const templateModal = document.querySelector("#modalConteiner");
const fragment = templateModal.content.cloneNode(true);
const searchForm = templateFilterForm.content.cloneNode(true);
const formModal = fragment.querySelector(".modal");
const modalContent = formModal.querySelector(".modal-body");
const titleModal = formModal.querySelector(".titleModal");

const container = document.createElement("div");
container.classList.add("container-fluid");
const row = document.createElement("div");
row.classList.add("row");

export {
  URL,
  root,
  searchForm,
  formModal,
  modalContent,
  container,
  row,
  titleModal,
};

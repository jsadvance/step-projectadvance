import { userLogin } from "./functions.js";
import { Request } from "./request.js";

const request = new Request();

class Modal {
  render() {
    let form = document.createElement("form");

    const emailInput = document.createElement("input");
    emailInput.type = "email";
    emailInput.className = "input-login";
    emailInput.placeholder = "Електронна пошта";
    emailInput.id = "email";

    const passwordInput = document.createElement("input");
    passwordInput.type = "password";
    passwordInput.className = "input-password";
    passwordInput.placeholder = "Пароль";
    passwordInput.id = "password";

    const submitButton = document.createElement("button");
    submitButton.type = "submit";
    submitButton.className = "btn btn-primary btn-lg";
    submitButton.textContent = "Увійти";
    submitButton.setAttribute("data-bs-dismiss", "modal");

    form.addEventListener("submit", (element) => {
      element.preventDefault();
      userLogin(emailInput, passwordInput, form);
    });

    form.append(emailInput, passwordInput, submitButton);

    return form;
  }
}

export { Modal };

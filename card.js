'use strict'
import { Request } from './request.js'
import { arrCard, firstInfo } from './functions.js'
import { modalContent, root, titleModal } from './constants.js'
import { Visit } from './visits.js'

class Card {
  renderCard(obj) {
    let {
      id,
      plannedDate,
      priority,
      status,
      patientName,
      patientLastName,
      doctorName,
      doctorLastName,
      title,
      description,
      doctor,

      pressure,
      imt,
      illnesses,
      age,
      lastVisitDate,
    } = obj

    const cardTemplate = document.querySelector('#card').content
    const card = cardTemplate.querySelector('.visit-card-list').cloneNode(true)

    const editBtn = card.querySelector('.edit-btn')
    const deleteBtn = card.querySelector('.delete-btn')

    const idElement = card.querySelector('.id')
    const plannedDateElement = card.querySelector('.planned-date')
    const priorityElement = card.querySelector('.priority')
    const statusElement = card.querySelector('.status')
    const patientNameElement = card.querySelector('.patientName')
    const doctorNameElement = card.querySelector('.doctorName')
    const cardTitleElement = card.querySelector('.card-title')
    const cardTextElement = card.querySelector('.card-text')
    const doctorTypeElement = card.querySelector('.doctorType')

    const additionalInfo = card.querySelector('.card-additional-info')
    const additionalInfoList = card.querySelector('.additional-info-data')

    this.changeCardColor(priority, status, card)

    idElement.textContent = id
    plannedDateElement.innerHTML = `Дата візиту: <strong>${plannedDate}</strong>`
    priorityElement.innerHTML = `Терміновість: <strong>${priority}</strong>`
    statusElement.innerHTML = `Статус: <strong>${status}</strong>`
    patientNameElement.innerHTML = `Пацієнт: <strong>${patientName} ${patientLastName}</strong>`
    doctorNameElement.innerHTML = `Доктор: <strong>${doctorName} ${doctorLastName}</strong>`
    cardTitleElement.textContent = title
    cardTextElement.textContent = description
    doctorTypeElement.innerHTML = `Спеціалізація лікаря: <strong>${doctor}</strong>`

    this.renderMoreInfo(obj, card)

    const showMoreLink = card.querySelector('.show-more')

    showMoreLink.addEventListener('click', (e) => {
      e.preventDefault()
      additionalInfo.classList.toggle('d-none')
      if (additionalInfo.classList.value.includes('d-none')) {
        showMoreLink.textContent = 'Показати більше'
      } else {
        showMoreLink.textContent = 'Показати менше'
      }
    })

    editBtn.addEventListener('click', () => {
      const visit = new Visit()
      modalContent.append(
        visit.render(
          id,
          status,
          doctor,
          title,
          doctorName,
          doctorLastName,
          description,
          patientName,
          patientLastName,
          priority,
          plannedDate,
          card,

          {
            pressure,
            imt,
            illnesses,
            age,
            lastVisitDate,
          }
        )
      )
      visit.newVisit.querySelector('.status').classList.remove('hidden')
      visit.newVisit.querySelector('#status').value = status
      titleModal.textContent = 'Редагування візиту до лікаря'
    })

    deleteBtn.addEventListener('click', () => {
      const request = new Request()
      request
        .deletePost(sessionStorage.getItem('token'), id)
        .then((response) => {
          card.remove()
          let delCard = arrCard.find((obj) => obj.id === id)

          arrCard.splice(delCard, 1)

          if (arrCard.length === 0) {
            root.append(firstInfo)
          }
        })
      const cardsLeft = [...document.querySelectorAll('.visit-card-list')]
      if (cardsLeft.length === 1) {
        firstInfo.className =
          'badge text-bg-warning text-wrap d-flex justify-content-center'
        firstInfo.style.fontSize = '30px'
        firstInfo.textContent = 'No items have been added'
      }
    })
    return card
  }

  renderMoreInfo(obj, card) {
    let { pressure, imt, illnesses, age, lastVisitDate } = obj

    const fragment = document.createDocumentFragment()

    const additionalInfoList = card.querySelector('.additional-info-data')
    let ageElement = document.createElement('li')
    let pressureElement = document.createElement('li')
    let imtElement = document.createElement('li')
    let illnessesElement = document.createElement('li')
    let lastVisitDateElement = document.createElement('li')

    const doctorProfession = obj.doctor

    switch (doctorProfession) {
      case 'Кардіолог':
        pressureElement.innerHTML = `Тиск: <strong>${pressure}</strong>`
        imtElement.innerHTML = `ІМТ: <strong>${imt}</strong>`
        ageElement.innerHTML = `Вік: <strong>${age}</strong>`
        illnessesElement.innerHTML = `Історія хвороб: ${illnesses}`

        fragment.append(
          pressureElement,
          imtElement,
          ageElement,
          illnessesElement
        )

        break

      case 'Терапевт':
        ageElement.innerHTML = `Вік: ${age}`
        fragment.append(ageElement)

        break

      case 'Дантист':
        lastVisitDateElement.innerHTML = `Попередній візит: ${lastVisitDate.replace(
          '-',
          '.'
        )}`
        fragment.append(lastVisitDateElement)

        break
    }

    additionalInfoList.append(fragment)

    return additionalInfoList
  }

  changeCardColor(priority, status, card) {
    const cardHeader = card.querySelector('.card-header')

    switch (priority) {
      case 'Звичайна':
        cardHeader.classList.add('text-bg-success')
        break

      case 'Пріоритетна':
        cardHeader.classList.add('text-bg-warning')
        break

      case 'Невідкладна':
        cardHeader.classList.add('text-bg-danger')
        break
    }

    if (status === 'Закритий') {
      cardHeader.classList.add('text-bg-light')

      const cardParagraphs = card.querySelectorAll('p')
      const cardSpans = card.querySelectorAll('span')
      const cardTextElements = [...cardParagraphs, ...cardSpans]
      cardTextElements.forEach((el) => el.classList.add('text-body-tertiary'))
    }
  }
}

export { Card }

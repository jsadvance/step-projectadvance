import {
  root,
  searchForm,
  modalContent,
  formModal,
  row,
  container,
  titleModal,
} from "./constants.js";
import { Modal } from "./modal.js";
import { Request } from "./request.js";
import { Card } from "./card.js";
import { Button, buttonContent, login } from "./script.js";
import { Visit } from "./visits.js";
import { Sort } from "./sort.js";

export const firstInfo = document.createElement("p");
export let arrCard = [];
const request = new Request();

root.append(formModal);

function userLoginModal() {
  let modal = new Modal();

  modalContent.append(modal.render());
  login.removeEventListener("click", userLoginModal);
}

function userLogin(emailInput, passwordInput, form) {
  if (emailInput.value !== "" && passwordInput.value !== "") {
    request
      .getToken(emailInput.value, passwordInput.value)
      .then((token) => {
        sessionStorage.setItem("token", token);
        form.remove();
        afterUserLogin(token, login);
      })
      .catch((error) => alert(error));
  } else alert("Введіть дані для авторизації");
}

function afterUserLogin(token, login) {
  login.remove();
  const newVisitButton = new Button();
  root.append(searchForm);
  buttonContent.append(newVisitButton.render("newVisit", "Створити візит"));
  const newVisit = document.querySelector("#newVisit");

  newVisit.addEventListener("click", (el) => {
    const visit = new Visit();
    modalContent.innerHTML = "";
    modalContent.append(visit.render());
    titleModal.textContent = "Новий визіт до лікаря";
  });

  request.getVisits(token).then((response) => {
    if (response.length === 0) {
      firstInfo.className =
        "badge text-bg-warning text-wrap d-flex justify-content-center";
      firstInfo.style.fontSize = "30px";
      firstInfo.textContent = "No items have been added";
      root.append(firstInfo);
    }

    arrCard.push(...response);

    allCardVisit(arrCard);
  });
}

function allCardVisit(response) {
  response.forEach((item) => {
    let card = new Card();

    row.prepend(card.renderCard(item));
  });

  container.append(row);
  root.append(container);

  const form = document.getElementById(`visitFilterForm`);
  form.addEventListener(`submit`, (e) => {
    e.preventDefault();
  });
  form.addEventListener(`change`, () => {
    filterCard(arrCard);
  });

  let inputLine = document.getElementById(`searchInput`);
  inputLine.addEventListener("keyup", (e) => {
    filterCard(arrCard);
  });
}

function filterCard(arr) {
  let inputLine = document.getElementById(`searchInput`);
  let select1 = document.getElementById(`statusSelect`);
  let select2 = document.getElementById(`urgencySelect`);
  const sort = new Sort();

  return sort.filterCardsArr(
    arr,
    inputLine.value,
    select1.value,
    select2.value
  );
}

function currentDate() {
  let currentDate = new Date();
  let year = currentDate.getFullYear();
  let month = (currentDate.getMonth() + 1).toString().padStart(2, "0");
  let day = currentDate.getDate().toString().padStart(2, "0");
  let hours = currentDate.getHours().toString().padStart(2, "0");
  let minutes = currentDate.getMinutes().toString().padStart(2, "0");

  return `${year}-${month}-${day} ${hours}:${minutes}`;
}

export {
  userLoginModal,
  userLogin,
  currentDate,
  afterUserLogin,
  allCardVisit,
  filterCard,
};
